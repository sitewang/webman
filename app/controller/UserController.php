<?php
/**
 * Created by PhpStorm.
 * Author: sitenv@aliyun.com
 * CreateTime: 2023/2/9 12:56
 * Blog：www.myblogs.xyz
 */

namespace app\controller;

use support\Request;

class UserController
{
    public function hello(Request $request)
    {
        $default_name = 'webman';
        $name = $request->get('name', $default_name);
        $route = $request->route;
        if ($route) {
            var_export($route->getPath());
            var_export($route->getMethods());
            var_export($route->getName());
            var_export($route->getMiddleware());
            var_export($route->getCallback());
            var_export($route->param()); // 此特性需要 webman-framework >= 1.3.16
        }
        return response('hello ' . $name);
    }

    public function helloJson(Request $request)
    {
        $default_name = 'webman';
        $name = $request->get('name', $default_name);
        return json([
            'code'=>0,
            'msg'=>'',
            'date'=>[
                'hello'=>$name
            ]
        ]);
    }
}
<?php
/**
 * Created by PhpStorm.
 * Author: sitenv@aliyun.com
 * CreateTime: 2023/2/11 16:13
 * Blog：www.myblogs.xyz
 */

namespace app\common\service;

class EncryptionService
{
    public function mobile($data)
    {
        $delimiter = chr("123") . chr("123");
        $result = explode($delimiter, $data);
        if (count($result) != 3 && count($result) != 2){
            return false;
        }
        if (count($result) == 2){
            $mobile = ($result['0'] - 321) / 123;
            return ['account'=>$mobile,'type'=>$result['1']];
        }
        return false;
    }
}
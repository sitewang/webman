<?php
/**
 * Created by PhpStorm.
 * Author: sitenv@aliyun.com
 * CreateTime: 2023/2/11 17:36
 * Blog：www.myblogs.xyz
 */

namespace app\common\model;

class Demo extends Common
{
    protected $table = 'demo';
    protected $primaryKey = 'id';
}
<?php
/**
 * Created by PhpStorm.
 * Author: sitenv@aliyun.com
 * CreateTime: 2023/2/9 13:41
 * Blog：www.myblogs.xyz
 */

namespace app\api\controller;

class IndexController extends CommonController
{
    public function index()
    {
        $this->require = ['name', 'sex', 'age'];
        if (!$this->verifyParam()) return $this->verifyError();
        return $this->jsonResult($this->data);
    }
}
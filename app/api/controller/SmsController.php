<?php
/**
 * Created by PhpStorm.
 * Author: sitenv@aliyun.com
 * CreateTime: 2023/2/11 16:19
 * Blog：www.myblogs.xyz
 */

namespace app\api\controller;

class SmsController extends CommonController
{
    public function index()
    {
        if ($this->verifyParam('account')) return $this->verifyError();
        return $this->jsonSuccess();
    }
}
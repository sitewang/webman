# webman

High performance HTTP Service Framework for PHP based on [Workerman](https://github.com/walkor/workerman).

# Manual (文档)

https://www.workerman.net/doc/webman

# Home page (主页)
https://www.workerman.net/webman

## LICENSE

MIT

## 安装配置内容查看composer.json文件配置

composer install